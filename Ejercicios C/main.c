#include <stdio.h>
#include <string.h>



#define PI 3.14159
#define AR_SIZE 10

void createMenu();

int main() {
    /*presentaNum();
    presentaRadio();
    numerosPantalla();
    printBubbleSort();*/
    createMenu();

    return 0;
}


void presentaNum() {
    for (int i = 1; i <= 10; ++i) {
        printf("%d",i);
        printf("\n");
    }
}

void presentaRadio() {
    float radio;
    double perimetro;
    float area;

    printf("Introduzca el radio");
    scanf("%f", &radio);


    perimetro = 2 * PI *radio;
    area = PI * radio + radio;

    printf("El perimetro del circulo es: %.2f\n", perimetro);
    printf("El area del circulo es: %.2f\n", area);
}

void numerosPantalla() {
    int arrayNum[AR_SIZE];
    printf("Introduce 10 numeros");
    for (int i = 0; i < AR_SIZE; ++i) {
        printf("Numero %d: ", i+1);
        scanf("%d", &arrayNum[i]);
        printf("%d", arrayNum[i]);
    }

    printf("Array: ");
    for (int i = 0; i < AR_SIZE; i++) {
        printf("%d ", arrayNum[i]);
    }
    printf("\n");
}

void bubbleSort(int array[], int size) {
    int i, j;
    for (i = 0; i < size-1; i++) {
        for (j = 0; j < size-i-1; j++) {
            if (array[j] > array[j+1]) {
                int temp = array[j];
                array[j] = array[j+1];
                array[j+1] = temp;
            }
        }
    }
}

void printArray(int array[], int size) {
    int i;
    for (i = 0; i < size; i++) {
        printf("%d ", array[i]);
    }
    printf("\n");
}

void printBubbleSort() {
    int array[10];
    int i;

    printf("Ingrese 10 numeros:\n");
    for (i = 0; i < 10; i++) {
        printf("Numero %d: ", i+1);
        scanf("%d", &array[i]);
    }

    printf("Array original: ");
    printArray(array, 10);
    bubbleSort(array, 10);
    printf("Array ordenado: ");
    printArray(array, 10);
}

void func_max() {
    int numeros[10];
    int i, maximo;

    for ( i = 0; i < 10; i++) {
        printf("Numero %d: ", i+1);
        scanf("%d", &numeros[i]);
    }

    maximo = numeros[0];

    for (i = 0; i < 10; i++) {
        if (numeros[i] > maximo) {
            maximo = numeros[i];
        }
    }
    printf("El numero maximo es: %d\n", maximo);
}

void createMenu() {
    char opcion[50];

    while (1) {
        printf("Menu:\n");
        printf("1. Numeros del 1 al 10\n");
        printf("2. Sacar el perimetro y area\n");
        printf("3. Recoger 10 numeros introducidos por el user\n");
        printf("4. Ordenaro 10 numeros introducidos por el user\n");
        printf("5. Sacar el max de 10 numeros introducidos por el user\n");
        printf("6. Salir\n");
        printf("Introduzca una opcion: ");
        scanf("%s", opcion);

        if ((strcmp(opcion, "1"))==0) {
            presentaNum();
        } else if ((strcmp(opcion, "2"))==0)  {
            presentaRadio();
        } else if ((strcmp(opcion, "3"))==0) {
            numerosPantalla();
        } else if ((strcmp(opcion, "4"))==0)  {
            printf("Introduce 10 numeros");
            printBubbleSort();
        } else if ((strcmp(opcion, "5"))==0)  {
            printf("Introduce 10 numeros");
            func_max();
        } else if ((strcmp(opcion, "6"))==0)  {
            printf("Saliendo del programa...\n");
            break;
        } else {
            printf("Opcion invalida. Por favor, Introduzca una opcion valida.\n");
        }
    }
}