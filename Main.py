def buscar_fichero():
    fichero = "C:/Users/adrian.ponce/Desktop/Cursos/Ejercicios/coches.txt"
    return fichero

def add_register():
    print("Se va a añadir un registro")
    id_user = input("Introduce el ID: ")
    nombre = input("Introduce el nombre: ")
    edad = input("Introduce la edad del usuario: ")
    marca = input("Introduce la marca del coche: ")
    modelo = input("Introduce el modelo del coche: ")

    registro = f"\n{id_user} {nombre} {edad} {marca} {modelo}"

    with open(buscar_fichero(), "a") as archivo:
        archivo.write(registro)


def mostrar_nombres():
    cadena = input("Introduce el nombre que quieres buscar:")

    try:
        fichero = open(buscar_fichero(), "r")

        for linea in fichero:
            palabras = linea.split()
            palabras = str(palabras[1])
            if cadena.lower() == palabras.lower():
                print(" ", linea)

        fichero.close()
    except IOError:
        print("No ha funcionado")


def mostrar_mayores():
    print("Los registros con usuarios mayores de edad son: ")

    try:
        fichero = open(buscar_fichero(), "r")

        for linea in fichero:
            palabras = linea.split()
            palabras = (palabras[2])
            if palabras >= "18":
                print(" ", linea)

        fichero.close()

    except IOError:
        print("No ha funcionado")


while True:
    print("selecciona una opcion: ")
    print("1 - Añadir registro")
    print("2 - Mostrar Nombre o Mayores de edad")
    print("3 - salir")

    x = input()
    print(x)

    if x == "1":
        add_register()

    if x == "2":
        print("opcion 2")
        while True:
            print("Selecciona una opcion: ")
            print("1 - Mostrar todos los registros con el nombre indicado ")
            print("2 - Mostrar todos los usuarios mayores de edad")
            print("3 - Salir")

            y = input()
            print(y)

            if y == "1":
                mostrar_nombres()
            elif y == "2":
                mostrar_mayores()
            elif y == "3":
                break
            else:
                print("No has introducido una opcion válida")

    elif x == "4":
        break
    else:
        print("No has introducido una opcion válida")
